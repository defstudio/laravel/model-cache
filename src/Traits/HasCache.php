<?php /** @noinspection DuplicatedCode */


namespace DefStudio\ModelCache\Traits;


use Cache;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait HasCache
 *
 * @property string $cache_prefix
 * @package App\Traits
 * @mixin Model
 */
trait HasCache
{
    public bool $cache_enabled = true;

    public static function bootHasCache()
    {

        /** @phpstan-ignore-next-line */
        self::updating(function (Model|HasCache $model) {
            /** @phpstan-ignore-next-line */
            $model->check_caches_to_be_invalidated();
        });

        /** @phpstan-ignore-next-line */
        self::creating(function (Model|HasCache $model) {
            /** @phpstan-ignore-next-line */
            $model->check_caches_to_be_invalidated();
        });
    }

    public function check_caches_to_be_invalidated()
    {
        if (empty($this->invalidate_caches)) return;

        foreach ($this->invalidate_caches as $changed_field => $invalidation_targets) {
            if ($changed_field !== '*' && !$this->wasChanged($changed_field)) continue;

            foreach ($invalidation_targets as $this_field => $cache_key) {
                if ($this_field == 'self') {
                    $this->invalidate_cache($cache_key);
                } else {
                    $this->$this_field->invalidate_cache($cache_key);
                }
            }
        }
    }

    public function invalidate_cache($key)
    {
        Cache::forget($this->cache_key($key));
    }

    protected function cache_key($key): string
    {
        return "{$this->cache_prefix}_{$key}_{$this->id}";
    }

    protected function cache_store(string $key, string $value, CarbonInterface $ttl = null)
    {
        Cache::put($this->cache_key($key), $value, $ttl);
    }

    protected function cache_remember(string $key, \Closure $callback, CarbonInterface $ttl = null)
    {
        if ($this->cache_enabled) {

            if (empty($ttl)) {
                return Cache::rememberForever($this->cache_key($key), $callback);
            }

            return Cache::remember($this->cache_key($key), $ttl, $callback);
        } else {
            return $callback();
        }

    }
}
