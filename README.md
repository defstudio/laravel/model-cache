##### **Description**

Save time when loading a page. This is what happens after the code has been loaded and executed the first time because it saves the data in the cache and it can also get them back, avoiding many queries to run that could be repetitive. At the moment of any modification of the model the cache is invalidated. At the first request, the data is recalculated.

##### **Installation**

include the custom repository in composer.json:

    "repositories": [

    {

        "type": "vcs",

        "url": "https://gitlab.com/defstudio/laravel/model-cache.git"

    },

    ]  

and require via composer:

    composer require defstudio/model-cache

##### **Usage**

In your file Model:

    use DefStudio\ModelCache\Traits\HasCache;


